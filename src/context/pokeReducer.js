export const pokeReducer = (state, action) => {
    switch(action.type){
        case "FETCH" :
            return action.pokemon
        default:
            return state
    }
}