import { useLazyQuery } from '@apollo/client';
import axios from 'axios';
import React, {createContext, useState, useReducer, useEffect, useCallback} from 'react'
import { GET_POKEMONS } from '../queries/query';
import {pokeReducer} from './pokeReducer'

export const PokemonContext = createContext();

const PokemonProvider = ({children}) => {
    const [Pokemon, dispatch] = useReducer(pokeReducer, [])
    const [Offset, setOffset] = useState(0)
    const [Result, setResult] = useState([])
    const [Data, setData] = useState([])
    const abortController = new AbortController()
    const [getPokemon, {loading, data = null}] = useLazyQuery(GET_POKEMONS, {
        variables: {
          limit: 40,
          offset: Offset,
        },
        context: {
            fetch: {
                AbortSignal: abortController
            }
        }
    });
    const reloadPokemon = useCallback(
        () => {
            axios.get('https://us-central1-zicare-31c83.cloudfunctions.net/webApi/pokemon').then((response)=>{
                const data = response.data.sort((a, b) => (a.data.id_pokemon > b.data.id_pokemon) ? 1 : -1)
                dispatch({type:'FETCH', pokemon:data})
            })
        },
        [],
    )
    useEffect(() => {
        reloadPokemon()
    }, [])
    useEffect(() => {
        getPokemon()
    }, [])
    useEffect(() => {
        if(data) setData((Data)=>[...Data, ...data.pokemons.results])
    }, [data])
    useEffect(() => {
        const arr = [];
        Pokemon.forEach(item=>{
            if(arr.find(id => item.data.id_pokemon === id["id_pokemon"])){
                return;
            }
            arr.push({id_pokemon: item.data.id_pokemon, count: Pokemon.filter(ids => ids.data.id_pokemon === item.data.id_pokemon).length})
        })
        arr.sort((a,b) => a.Count - b.Count);
        if(Data != null){
            const test = Data.map(t1 => ({...t1, ...arr.find(t2 => t2.id_pokemon == t1.id)}))
            setResult(test)
        }
    }, [Data, Pokemon])
    const handleOffset = ()=>{
        abortController.abort()
        setOffset((Offset)=>Offset+40)
    }
    
    return (
        <PokemonContext.Provider value={{ Pokemon, reloadPokemon, Offset, handleOffset, Result, loading}}>
            {children}
        </PokemonContext.Provider>

    )
}
export default PokemonProvider;