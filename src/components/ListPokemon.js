import {useContext, useState} from 'react'
import { IconButton, List, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import axios from 'axios'
import React from 'react'
import { PokemonContext } from '../context/pokeContext'
import { releaseIcon } from '../assets'
import LoadingBar from './fragment/LoadingBar'

function ListPokemon() {
    const {reloadPokemon, Pokemon} = useContext(PokemonContext)
    const [Show, setShow] = useState(false)
    const [Load, setLoad] = useState(0)
    const handleDelete = (id)=>{
        setLoad(0)
        setShow(true)
        const delPokemon = new URLSearchParams({nickname: id})
        axios.post('https://us-central1-zicare-31c83.cloudfunctions.net/webApi/delete', delPokemon).then((response)=>{
            reloadPokemon()
            setLoad(2)
            setTimeout(() => {
                setShow(false)
            }, 500);
        }).catch((error)=>{
            setLoad(1)
            setTimeout(() => {
                setShow(false)
            }, 500);
        })
    }
    return (
        <div>
            <List>
                {
                    Pokemon.map((ea)=>(
                        <ListItem divider>
                            <ListItemText
                                primary={ea.id}
                                secondary={`Pokemon : ${ea.data.name}`}
                            />
                            <ListItemSecondaryAction>
                                <IconButton onClick={()=>handleDelete(ea.id)}>
                                    <img src={releaseIcon} style={{width:40, height:40}} />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))
                }
            </List>
            <LoadingBar show={Show} handleClose={()=>setShow(false)} loaded={Load} title={'Release'} />
        </div>
    )
}

export default ListPokemon
