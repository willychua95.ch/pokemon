import React, { useContext, useRef, useCallback, useState} from 'react'
import { ListItem, ListItemText, List, ListItemAvatar, Avatar, Grid, Paper, Typography, makeStyles, Button, Tooltip, Fab } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { PokemonContext } from '../context/pokeContext';
import { FormatListNumbered, GridOn, Menu } from '@material-ui/icons';

const useStyles = makeStyles((theme)=>({
  paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
  },
  fab: {
    position: 'fixed',
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
  },
}))

function Home() {
  const {handleOffset, loading, Result} = useContext(PokemonContext)
  const [isGrid, setisGrid] = useState(true)
  const history = useHistory();
  const divRef = useRef()
  const styles = useStyles();
  const observer = useRef();
  const lastPokemonRef = useCallback(
    (node) => {
      if(loading) return;
      if(observer.current) observer.current.disconnect()
      observer.current = new IntersectionObserver(entries=>{
        if(entries[0].isIntersecting){
          handleOffset()
        }
      })
      if(node) observer.current.observe(node)
    },
    [loading],
  )
  return (
    <div ref={divRef}>
      
      {isGrid ? (
      <Grid container spacing={3}>
        {Result && Result.map((ea, index)=>{
          if(Result.length === index+1){
            return(
              <Grid item xs="6" sm="4" md="3" lg="2" ref={lastPokemonRef} onClick={()=>history.push(`/detail/${ea.name}`)}>
                <Paper className={styles.paper}>
                  <img src={ea.image} style={{width:'100%'}} />
                  <Grid item xs>
                    <Typography noWrap style={{textTransform:'capitalize'}}>{ea.name}</Typography>
                    <h4>Total Owned : {ea.count ?? '0'}</h4>
                  </Grid>
                </Paper>           
              </Grid>
            )
          }else{
            return(
              <Grid item xs="6" sm="6" md="4" lg="2" onClick={()=>history.push(`/detail/${ea.name}`)}>
                <Paper className={styles.paper}>
                  <img src={ea.image} style={{width:'100%'}} />
                  <Grid item xs>
                    <Typography noWrap style={{textTransform:'capitalize'}}>{ea.name}</Typography>
                    <h4>Total Owned : {ea.count ?? '0'}</h4>
                  </Grid>
                </Paper>         
              </Grid>
            )
          }
          
        })}
      </Grid>
      ):(
      <List>
        {
          Result && Result.map((ea, index)=>{
              if(Result.length === index+1){
                return (
                <ListItem button divider onClick={()=>history.push(`/detail/${ea.name}`)} ref={lastPokemonRef}>
                  <ListItemAvatar>
                    <Avatar>
                      <img src={ea.image} style={{width:40, height:40}} alt="pokemon" />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={ea.name} secondary={`Total owned : ${ea.count ?? '0'}`} style={{textTransform:'capitalize'}} />
                </ListItem>
                )
              }else{
                return (
                <ListItem button divider onClick={()=>history.push(`/detail/${ea.name}`)}>
                  <ListItemAvatar>
                    <Avatar>
                      <img src={ea.image} style={{width:40, height:40}} alt="pokemon" />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={ea.name} secondary={`Total owned : ${ea.count ?? '0'}`} style={{textTransform:'capitalize'}} />
                </ListItem>
                )
              }
                
          })
          
        }
        <ListItem>
          {loading && "Fetch Data"}

        </ListItem>
      </List>
      )}
      <Tooltip title="Show Pokemon">
        <Fab color="primary" aria-label="add" className={styles.fab} onClick={()=>setisGrid(!isGrid)}>
          {isGrid ? <FormatListNumbered /> : <GridOn />}
        </Fab>
      </Tooltip>
    </div>
  )
  
}

export default Home;
