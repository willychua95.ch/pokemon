import {useContext, useState,} from 'react'
import { useLazyQuery, useQuery } from '@apollo/client'
import { Backdrop, Button, CircularProgress, Fab, Fade, Grid, List, ListItem, ListItemText, makeStyles, Modal, Tooltip, } from '@material-ui/core';
import React from 'react'
import { useParams } from 'react-router';
import { PokemonContext } from '../context/pokeContext';
import { GET_MOVE, GET_POKEMON } from '../queries/query';
import LoadingBar from './fragment/LoadingBar';
import axios from 'axios'
import { pokeballIconColor } from '../assets';

const useStyles = makeStyles((theme) => ({
    fab: {
        position: 'fixed',
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 20,
        left: 'auto',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        margin: 20
    },
    textfield: {
        width: '100%',
        padding: '12px 20px',
        margin: '8px 0',
        boxSizing: 'border-box',
        borderRadius: 4,
    },
    itemGrid: {
        backgroundColor:'#3f51b5', 
        color:'white', 
        padding: '7px 10px', 
        borderRadius: 10, 
        width: 'fit-content', 
        margin: '0 5px', 
        textTransform:'capitalize'
    }
}))

function Detail() {
    const styles = useStyles();
    const [Open, setOpen] = useState(false)
    const [Loading, setLoading] = useState(false)
    const [Catch, setCatch] = useState(0)
    const [Name, setName] = useState("")
    const [isAdded, setisAdded] = useState(false)
    const [Error, setError] = useState("")
    const [OpenMove, setOpenMove] = useState(false)
    const [isShiny, setisShiny] = useState(false)
    const {reloadPokemon, Pokemon} = useContext(PokemonContext)
    const [Move, setMove] = useState()
    const {name} = useParams();
    const [getMove, dataMove] = useLazyQuery(GET_MOVE, {
        variables: {
            move: Move
        }
    })
    const {loading, error, data} = useQuery(GET_POKEMON, {
        variables: {
            name: name
        }
    })
    if(loading) return <div style={{position:'absolute', top:'45%', left:'45%'}}><CircularProgress /></div>
    if(error) return `error ${error.message}`
    const handleAdd = ()=>{
        setisAdded(true)
        const checkNickname = Pokemon.find((ea)=>ea.id == Name)
        if(checkNickname){
            setError('nickname already exists')
            setisAdded(false)
            return;
        }
        const newPokemon = new URLSearchParams({
            name: data.pokemon.name,
            id: data.pokemon.id,
            nickname: Name,
        })
        axios.post('https://us-central1-zicare-31c83.cloudfunctions.net/webApi/insert', newPokemon).then((response)=>{
            if(!response.data.status){
                setError(response.data.message)
                setisAdded(false)
                return;
            }
            reloadPokemon()
            setOpen(false)
            setisAdded(false)
        }).catch((error)=>{
            console.log(error)
        })
    }
    const handleCatch = ()=>{
        setCatch(0)
        setLoading(true)
        setTimeout(() => {
            const prob = Math.floor(Math.random() * 2) + 1
            setCatch(prob)
        }, 1000)
    }
    const handleOpen = ()=>{
        setLoading(false)
        if(Catch === 2){
            setName("")
            setOpen(true)
        }else{
            setOpen(false)
        }
    }
    const handleMove = (name)=>{
        setMove(name)
        getMove()
        setOpenMove(true)
    }
    return (
        <div style={{maxWidth:500, marginRight:'auto', marginLeft:'auto'}}>
            <h2 style={{textAlign:'center', textTransform:'capitalize'}}>{data.pokemon.name}</h2>
            <div style={{textAlign: 'center', marginBottom: 20,}}>
                <img src={isShiny ? data.pokemon.sprites.front_shiny : data.pokemon.sprites.front_default} style={{width: '100%', border: '1px solid #ccc'}} alt="Pokemon Image" />
                <Button variant='contained' onClick={()=>setisShiny(!isShiny)} style={{backgroundColor: isShiny ? '#f4f4f4':'#ffd700'}}>{isShiny ? 'Normal' : 'Shiny'}</Button>
            </div>
            <div style={{display:'flex',backgroundColor:'#3f51b5', borderRadius: 10,textAlign:'center', color:'white', width:'100%'}}>
                <div style={{flex:1}}>
                    <p>Weight : <br />{data.pokemon.weight}</p>
                    <p>Abilities : <br /> {data.pokemon.abilities.map((ea)=><label style={{textTransform:'capitalize'}}>{ea.ability.name} <br /></label>)}</p>
                </div>
                <div style={{flex:1}}>
                    <p>Height : <br /> {data.pokemon.height}</p>
                    <p>Exp : <br />{data.pokemon.base_experience} xp</p>
                </div>
            </div>
            <h4>Types</h4>
            <div style={{display: 'flex'}}>
                {data.pokemon.types.map((ea)=>(
                    <Grid item className={styles.itemGrid}>
                        {ea.type.name}
                    </Grid>
                ))}
            </div>
            <h4>Moves</h4>
            <div style={{height:170, overflow: 'auto'}}>
                <List>
                    {data.pokemon.moves.map((ea)=>(
                        <ListItem button divider onClick={()=>handleMove(ea.move.name)}>
                            <ListItemText style={{textTransform:'capitalize'}}>{ea.move.name}</ListItemText>
                        </ListItem>
                    ))}
                </List> 
            </div>
            <Tooltip title="Catch Pokemon">
                <Fab color="primary" aria-label="add" className={styles.fab} onClick={handleCatch}>
                    <img src={pokeballIconColor} style={{width: 40, height: 40}} alt="pokeballicon" />
                </Fab>
            </Tooltip>
            <Modal
                open={Open}
                className={styles.modal}
                closeAfterTransition
                BackdropComponent = {Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={Open}>
                    <div className={styles.paper}>
                        <h2>Pokemon</h2>
                        <label>Nickname : </label>

                        <input type="text" className={styles.textfield} value={Name} onChange={(e)=>setName(e.target.value)} />
                        <p style={{color:'red', margin:0}}>{Error}</p>
                        
                        {isAdded ? <CircularProgress></CircularProgress> : <Button variant="contained" color="primary" onClick={handleAdd}>Add</Button> }
                    </div>
                </Fade>
                
            </Modal>
            <LoadingBar show={Loading} handleClose={()=>handleOpen()} loaded={Catch} title={'Catch Pokemon'} />
            <Modal
                open={OpenMove}
                className={styles.modal}
                closeAfterTransition
                BackdropComponent = {Backdrop}
                onClose={()=>setOpenMove(false)}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={OpenMove}>
                    <div className={styles.paper} style={{width:400}}>
                        
                        {
                            dataMove.loading || !dataMove.data ? <CircularProgress /> : (
                                <div>
                                <h2>Move {dataMove.data.move.response.name}</h2>
                                <p>Power : {dataMove.data.move.response.power ?? '-'}</p>
                                <p>PP : {dataMove.data.move.response.pp}</p>
                                <p>Accuracy : {dataMove.data.move.response.accuracy ?? '-'}</p>
                                <Button variant={'contained'} color={'primary'} onClick={()=>setOpenMove(false)}>OK</Button>
                                </div>
                            )
                        }
                    </div>
                </Fade>
                
            </Modal>
        </div>
    )
}

export default Detail
