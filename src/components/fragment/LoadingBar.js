import { Button, CircularProgress, makeStyles, Modal } from '@material-ui/core';
import React from 'react';
import { checkmarkIcon, multiplyIcon } from '../../assets';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        alignItems: 'center',
        justifyContent: 'center',
        display:'flex',
        flexDirection: 'column',
        margin: 20,
        minWidth: 200,
    },
}))

export default function LoadingBar({show = false, handleClose, loaded = 0, title = ""}){
    const styles = useStyles();
    return(
        <Modal open={show} className={styles.modal} aria-labelledby="simple-modal-title"aria-describedby="simple-modal-description">
            <div className={styles.paper}>
                <h2>{title}</h2>
                {loaded == 2 ? <img src={checkmarkIcon} style={{width:40, height:40}} alt="notification" /> : loaded == 1 ? <img src={multiplyIcon} style={{width:40, height:40}} alt="notification" />  :<CircularProgress />}
                <label>{loaded == 2 ? "Success" : loaded == 1 ? "Fail" : "Loading...."}</label>
                <Button variant="contained" color="primary" onClick={handleClose} style={{marginTop:15}}>OK</Button>
            </div>
        </Modal>
    )
}