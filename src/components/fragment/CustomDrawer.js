import { Divider, Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText, makeStyles } from '@material-ui/core';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import React from 'react'
import { Link } from 'react-router-dom';
import { pokeballIcon, pokedexIcon } from '../../assets';

const useStyles = makeStyles((theme) => ({
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      textDecoration: 'none',
      color: 'white',
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: 'flex-start'
    },
  }));

function CustomDrawer({isOpen, handleOpen, handleClose}) {
    const styles = useStyles();
    return (
        <Drawer anchor={'right'} open={isOpen} onClose={handleClose}>
            <div style={{width:250}}>
                <div className={styles.drawerHeader}>
                    <IconButton onClick={handleClose}>
                    {isOpen ? <ChevronLeft /> : <ChevronRight />}
                    </IconButton>
                </div>
                <Divider />
                <List>
                    <Link to='/' style={{textDecoration:'none', color:'black'}} onClick={handleClose}>
                        <ListItem divider>
                            <ListItemIcon><img src={pokedexIcon} style={{width:25, height:25}} /></ListItemIcon>
                            <ListItemText>Pokedex</ListItemText>
                        </ListItem>
                    </Link>
                    
                    <Link to='/listpokemon' style={{textDecoration:'none', color:'black'}} onClick={handleClose}>
                        <ListItem divider>
                            <ListItemIcon><img src={pokeballIcon} style={{width:25, height:25}} /></ListItemIcon>
                            <ListItemText>My Pokemon</ListItemText>
                        </ListItem>
                    </Link>
                </List>
            </div>
        </Drawer>
    )
}

export default CustomDrawer
