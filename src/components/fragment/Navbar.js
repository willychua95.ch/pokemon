import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core'
import { KeyboardBackspace, Menu } from '@material-ui/icons'
import React from 'react'

function Navbar({location, handleBack, title='Pokemon', handleOpen}) {
    return (
        <AppBar position="sticky">
            <Toolbar>
                {location == 'detail' ? 
                <IconButton style={{color:'white', marginRight: 10}} onClick={handleBack}>
                    <KeyboardBackspace />
                </IconButton>
                :''}
                <Typography variant="h6" style={{flexGrow:1}}>
                    {title}
                </Typography>
                <IconButton style={{color:'white'}} onClick={handleOpen}>
                  <Menu />
                </IconButton>
            </Toolbar>
        </AppBar>
    )
}

export default Navbar
