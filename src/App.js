import {useState, useEffect} from 'react'
import { Route, Switch, useHistory, useLocation } from 'react-router-dom';
import Home from './components/Home';
import Detail from './components/Detail';
import PokeProvider from './context/pokeContext'
import { Container, } from '@material-ui/core';
import ListPokemon from './components/ListPokemon';
import React from 'react';
import Navbar from './components/fragment/Navbar'
import CustomDrawer from './components/fragment/CustomDrawer';

function App() {
  const history = useHistory();
  const location = useLocation().pathname.split('/');
  const [isOpen, setisOpen] = useState(false)
  const [isBack, setisBack] = useState(false)
  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setisOpen(open);
  };
  useEffect(() => {
    if(location[1] == 'detail'){
      setisBack(()=>true)
    }
    setisBack(()=>false)
  }, [location])
  return(
      <div>
        <React.Fragment>
          <div style={{flexGrow:1}}>
            <Navbar location={location[1]} handleBack={()=>history.goBack()} title={location[1] == 'listpokemon' ? 'My Pokemon' : 'Pokedex'} handleOpen={toggleDrawer(true)} />
            <PokeProvider>
              <Container fixed>
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route path="/detail/:name" component={Detail} />
                  <Route path="/listpokemon" component={ListPokemon} />
                </Switch>
              </Container>
              
            </PokeProvider>
          </div>
          <CustomDrawer isOpen={isOpen} handleOpen={toggleDrawer(true)} handleClose={toggleDrawer(false)} />
        </React.Fragment>
      </div>
    
    
  )
  
}

export default App;
