import gql from "graphql-tag";

export const GET_POKEMON = gql `
    query pokemon($name: String!) {
        pokemon(name: $name) {
            id
            name
            weight
            height
            base_experience
            abilities {
              ability {
                name
              }
            }
            moves {
                move {
                  url
                  name
                }
            }
            types {
                type {
                    name
                }
            }
            sprites {
                front_default
                front_shiny
            }
        }
    }
`;

export const GET_POKEMONS = gql`
  query pokemons($limit: Int, $offset: Int) {
    pokemons(limit: $limit, offset: $offset) {
      count
      next
      previous
      status
      message
      results {
        id
        name
        image
      }
    }
  }
`;

export const GET_MOVE = gql`
  query move($move: String!) {
    move(move: $move){
      params
      response
    }
  }
`