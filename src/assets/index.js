import checkmarkIcon from './icons/checkmark.png'
import multiplyIcon from './icons/multiply.png'
import pokeballIcon from './icons/pokeball_icon.png'
import pokeballIconColor from './icons/pokeball.png'
import pokedexIcon from './icons/pokedex.png'
import releaseIcon from './icons/release.png'

export {checkmarkIcon, multiplyIcon, pokeballIcon, pokeballIconColor, pokedexIcon, releaseIcon}